Redhat-SSO Supported Version: 7.2.x

    [+] rhsso_management_console_user            # EAP Management Console User (That will be created)
    [+] rhsso_management_console_pass            # EAP Management Console Password (That will be created)    
    [+] rhsso_auth_console_pass                  # RHSSO Auth Console Password (That will be created)    
    [+] become_user_var                          # Operating System User Account for root
    [+] become_method_var                        # Operating System Command To escalate priviledge to root
    [+] running_user                             # Operating System User Account that Redhat-SSO will be running under
    [+] running_group                            # Operating System User Group that Redhat-SSO will be running under
    [+] rhsso_azure_ping_storage_account_name    # Azure Storage account name for standalone cluster purpose
    [+] rhsso_azure_ping_storage_access_key      # Azure Storage access key for standalone cluster purpose
    [+] rhsso_azure_ping_container               # Azure Storage container name for standalone cluster purpose
    [+] spi_mode                                 # EAP System Properties for environment mode
    [+] spi_random_key                           # EAP System Properties for random key
    [+] spi_random_value                         # EAP System Properties for random value
    [+] ds_username                              # Postgres database username
    [+] vault_attribute_value                    # Postgres database password 
    [+] ds_connection                            # Postgres Database Connection String, jdbc:postgresql://IP:PORT/DB
    [+] ods_username                             # ODS database username
    [+] vault_attribute_value_2                  # ODS database vault password
    [+] ods_connection                           # ODS database Connectin String, jdbc:db2://IP:PORT/DB
